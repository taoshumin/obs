/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package obs

import (
	"bytes"
	"github.com/huaweicloud/huaweicloud-sdk-go-obs/obs"
	"log"
	"os"
	"sync/atomic"
)

type Obs interface {
	CreateBucket(bucketName string) error
	DeleteBucket(bucketName string) error
	ExistBucket(bucketName string) error
	PutObject(bucketName, dir string, body []byte) error
	PutObjectWithFile(bucketName, dir, fileName string) error
	DeleteObject(bucketName, dir string) error
	Close() error
}

type ob struct {
	client      *obs.ObsClient
	configValue atomic.Value
}

func NewObs(config Config) (Obs, error) {
	client, err := obs.New(config.AK, config.SK, config.Endpoint)
	if err != nil {
		return nil, err
	}
	o := &ob{client: client}
	o.configValue.Store(config)
	return o, nil
}

func NewMockObjs() Obs {
	o, err := NewObs(NewConfig())
	if err != nil {
		panic(err)
	}
	return o
}

func (o *ob) config() Config {
	return o.configValue.Load().(Config)
}

func (o *ob) CreateBucket(bucketName string) error {
	input := &obs.CreateBucketInput{}
	input.Bucket = bucketName
	input.StorageClass = obs.StorageClassWarm
	input.ACL = obs.AclPublicRead

	out, err := o.client.CreateBucket(input)
	if err != nil {
		return err
	}
	log.Printf("create bucket sucess, statusCode: %v requestId: %v", out.StatusCode, out.RequestId)
	return nil
}

func (o *ob) DeleteBucket(bucketName string) error {
	out, err := o.client.DeleteBucket(bucketName)
	if err != nil {
		return err
	}
	log.Printf("delete bucket sucess, statusCode: %v requestId: %v", out.StatusCode, out.RequestId)
	return nil
}

func (o *ob) ExistBucket(bucketName string) error {
	out, err := o.client.HeadBucket(bucketName)
	if err != nil {
		return err
	}
	log.Printf("exists bucket,statusCode: %v requestId: %v", out.StatusCode, out.RequestId)
	return nil
}

func (o *ob) PutObject(bucketName, dir string, body []byte) error {
	input := &obs.PutObjectInput{}
	input.Bucket = bucketName
	input.Key = dir
	input.Body = bytes.NewReader(body)
	out, err := o.client.PutObject(input)
	if err != nil {
		return err
	}
	log.Printf("put object,statusCode: %v requestId: %v", out.StatusCode, out.RequestId)
	return nil
}

// PutObjectWithFile copy from https://support.huaweicloud.com/sdk-go-devg-obs/obs_23_0510.html
// copy from https://github.com/huaweicloud/huaweicloud-sdk-go-obs/blob/master/main/obs_go_sample.go
func (o *ob) PutObjectWithFile(bucketName, dir, fileName string) error {
	input := &obs.PutObjectInput{}
	input.Bucket = bucketName
	input.Key = dir
	fd, _ := os.Open(fileName)
	input.Body = fd
	out, err := o.client.PutObject(input)
	if err != nil {
		return err
	}
	log.Printf("put object use filename,statusCode: %v requestId: %v", out.StatusCode, out.RequestId)
	return nil
}

func (o *ob) DeleteObject(bucketName, dir string) error {
	input := &obs.DeleteObjectInput{}
	input.Bucket = bucketName
	input.Key = dir
	out, err := o.client.DeleteObject(input)
	if err != nil {
		return err
	}
	log.Printf("delete object,statusCode: %v requestId: %v", out.StatusCode, out.RequestId)
	return nil
}

func (o *ob) Close() error {
	defer o.client.Close()
	return nil
}
