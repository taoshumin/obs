/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package obs

import "errors"

const (
	// DefaultEndpoint the default is the four districts of Beijing.
	// copy from https://developer.huaweicloud.com/endpoint?OBS
	DefaultEndpoint = "obs.cn-north-4.myhuaweicloud.com"
	// DefaultAK the default is the ak.
	// get the documentation of ak,sk. to see https://support.huaweicloud.com/qs-obs/obs_qs_0005.html
	// https://console.huaweicloud.com/iam/?agencyId=299de32e877244e2b948128e60402720&region=cn-east-3&locale=zh-cn#/mine/accessKey
	DefaultAK = "L1UIJ9MQEYSNNAOPW053"
	// DefaultSK the default is the sk.
	// User Name			Access Key Id			Secret Access Key
	// hid_1djl4o22mpt-zw3	L1UIJ9MQEYSNNAOPW053	yWibkmjj6ZpRQK4twR6ZRkFcl8ERi0wYcYr3Bga6
	DefaultSK = "yWibkmjj6ZpRQK4twR6ZRkFcl8ERi0wYcYr3Bga6"
)

type Config struct {
	Endpoint string `mapstructure:"endpoint" json:"endpoint"`
	AK       string `mapstructure:"ak" json:"ak"`
	SK       string `mapstructure:"sk" json:"sk"`
}

func NewConfig() Config {
	return Config{
		Endpoint: DefaultEndpoint,
		AK:       DefaultAK,
		SK:       DefaultSK,
	}
}

func (c Config) Validate() error {
	if c.Endpoint == "" {
		return errors.New("endpoint cannot be empty")
	}
	if c.AK == "" || c.SK == "" {
		return errors.New("ak sk cannot be empty")
	}
	return nil
}
