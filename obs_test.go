/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package obs

import (
	"testing"
)

func Test_ob_PutObjectWithFile(t *testing.T) {
	type fields struct {
		os Obs
	}
	type args struct {
		bucketName string
		dir        string
		fileName   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "obs-upload-file",
			fields: fields{
				os: NewMockObjs(),
			},
			args: args{
				bucketName: "edgetest",
				dir:        "2021-11-03/59322377.png",
				fileName:   "./test/59322377.png",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.fields.os.PutObjectWithFile(tt.args.bucketName, tt.args.dir, tt.args.fileName); (err != nil) != tt.wantErr {
				t.Errorf("PutObjectWithFile() error = %v, wantErr %v", err, tt.wantErr)
			}
			defer tt.fields.os.Close()
		})
	}
}
